# Trabalho Final - Probabilidade e Estatística Aplicadas

Base de dados: https://www.ibge.gov.br/estatisticas/sociais/protecao-social/10586-pesquisa-de-informacoes-basicas-municipais.html?=&&t=o-que-e

Conjunto de dados escolhido: Saúde
## PARTE I)
- Sexo:
    - Número total de cada sexo (masculino e feminino) com gráfico para exemplificar
    - Moda
- Idade:
    - Média
    - Moda
    - Mediana
    - Quartis
    - Decis
    - Amplitude
    - Variância
    - Desvio padrão
- Cor/raça:
    - Número total dentro de cada classe (branca, parda, preta, etc) considerando o sexo e geral (ambos os sexos);
        - Fazer gráficos
    - Moda entre as classes considerando o sexo e geral (ambos os sexos)
- Escolaridade:
    - Número total dentro de cada classe (graduado, especialista, mestre, doutor) considerando o sexo e geral (ambos os sexos)
        - Fazer gráficos
    - Moda entre as classes considerando o sexo e geral (ambos os sexos)

### Importante: Para cada um dos temas, faça o máximo de análises que puder sobre os dados 
Ex.:
- 50% dos homens com mais de 40 anos tem especialização;
- Mais de 30% das mulheres são formadas em pedagogia;
- A maioria dos mestres são brancos e homens.

## PARTE II)
